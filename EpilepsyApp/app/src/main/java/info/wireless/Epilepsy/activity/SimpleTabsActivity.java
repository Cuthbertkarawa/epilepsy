package info.wireless.Epilepsy.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import info.wireless.Epilepsy.R;
import info.wireless.Epilepsy.SApplication;
import info.wireless.Epilepsy.fragments.OneFragment;
import info.wireless.Epilepsy.fragments.TwoFragment;

public class SimpleTabsActivity extends AppCompatActivity implements SensorEventListener {
    private final String TAG = SimpleTabsActivity.class.getSimpleName();
    String Mkoa="";
    String wilaya="";
    String latitude="";
    String longitude="";

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private long lastTime = 0;
    private float lastX, lastY, lastZ;
    private static final int THRESHOLD = 1000;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView title,titleTop;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_tabs);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title= (TextView) findViewById(R.id.title);
        titleTop= (TextView) findViewById(R.id.titleTop);
        setSupportActionBar(toolbar);

        if(toolbar!=null){
            actionBar=getSupportActionBar();
            setSupportActionBar(toolbar);
            toolbar.setTitle("");
            titleTop.setText("");
            title.setText(getResources().getString(R.string.app_name));


        }
            viewPager = (ViewPager) findViewById(R.id.viewpager);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "DETAILS");
        adapter.addFragment(new TwoFragment(), "EDIT");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;

        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            long currentTime = System.currentTimeMillis();
            if ((currentTime - lastTime) > 100) {
                long diffTime = (currentTime - lastTime);
                lastTime = currentTime;
                float speed = Math.abs(x + y + z - lastX - lastY - lastZ)/ diffTime * 10000;
                Log.d("Sppeed",String.valueOf(speed));
                if (speed > THRESHOLD) {
                    getRandomNumber();
                    Locationfinder();
                    //sendsms();
                    SharedPreferences settings =getSharedPreferences(TwoFragment.EPILEPSYPREFS, Context.MODE_PRIVATE);
                    String name = settings.getString(TwoFragment.Name, "Empty");
                    String phone1 = settings.getString(TwoFragment.Phone1, "Empty");
                    String phone2 = settings.getString(TwoFragment.Phone2, "Empty");
                    String message=settings.getString(TwoFragment.Message,"Empty");


                   /* //Toast.makeText(getApplicationContext(),"Nimemaliza  ", Toast.LENGTH_LONG).show();
                    do{
                        if(!"".equals(Mkoa) && !"".equals(wilaya)){
                            String sms=message+" "+name+"  mkoa nilio angukia ni "+Mkoa+" Wilaya ya"+wilaya;
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phone1, null, sms, null, null);
                            smsManager.sendTextMessage(phone2, null, sms, null, null);
                            Toast.makeText(getApplicationContext(),Mkoa, Toast.LENGTH_LONG).show();
                        }
                    }while("".equals(Mkoa) && "".equals(wilaya));
 * */
               Log.d("Long",longitude);
                   if(!"".equals(Mkoa) && !"".equals(wilaya))
                    {
                     String sms="\n"+ message+" "+name+"\n"+"  Mkoa  : "+Mkoa+"\n"+"  Wilaya :"+wilaya;
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phone1, null, sms, null, null);
                            smsManager.sendTextMessage(phone2, null, sms, null, null);
                        Toast.makeText(getApplicationContext(),"Alter SMS sent with Location", Toast.LENGTH_LONG).show();

                    }else
                    if(!"".equals(latitude) && !"".equals(longitude))
                    {
                        String sms=message+" "+name+" ."+"Mtandao unasumbua mgonjwa yuko."+"\n"+" Latitude: "
                                +latitude+"\n"+"Longitude:"+longitude+"\n"+"Tunashindwa kupata jina la eneo.";
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phone1, null, sms, null, null);
                        smsManager.sendTextMessage(phone2, null, sms, null, null);
                        Toast.makeText(getApplicationContext(),"Alter SMS sent with Longitude", Toast.LENGTH_LONG).show();
                    }

                }
                lastX = x;
                lastY = y;
                lastZ = z;
            }
        }
    }
    /*
      It's a good practice to unregister the sensor when the application hibernates to save battery power.
   */
    protected void onPause() {
        super.onPause();
         mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        //mSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void getRandomNumber() {
        Random randNumber = new Random();
        int iNumber = randNumber.nextInt(100);
        Log.d("Number",String.valueOf(iNumber));

    }

    public void  Locationfinder() {
        Log.d("KAKAKKA","YAHOOOO");
        if (SApplication.LOCATION != null) {
            double lat = SApplication.LOCATION.getLatitude();
            double lon = SApplication.LOCATION.getLongitude();
            latitude=String.valueOf(lat);
            latitude=latitude.substring(0,10);
           longitude=String.valueOf(lon);
            longitude=longitude.substring(0,10);

           // coordinates.setText(lat + " " + lon);
            Geocoder geocoder = new Geocoder(getApplicationContext(), new Locale("en"));
            try {
                // get address from location
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
                Log.d("Address", addresses.toString());
                String mm[] = addresses.toString().split(",");
                for (int j = 2; j < mm.length; j++) {
                    Log.d("kata", mm[j]);
                        Mkoa=mm[2].substring(8);
                        wilaya=mm[3].substring(6);
                }
                if (addresses != null && addresses.size() != 0) {
                    StringBuilder builder = new StringBuilder();
                    Address returnAddress = addresses.get(0);
                    for (int i = 0; i < returnAddress.getMaxAddressLineIndex(); i++) {
                        builder.append(returnAddress.getAddressLine(i));
                        builder.append(" ");
                    }
                } else {
                    Log.e(TAG, "Addresses null");
                }
            } catch (IOException e) {
                Log.e(TAG, "Geocoder exception " + e);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Check GPS status and internet connection", Toast.LENGTH_SHORT).show();
        }
    }


        @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
