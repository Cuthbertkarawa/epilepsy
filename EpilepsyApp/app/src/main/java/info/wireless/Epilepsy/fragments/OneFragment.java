package info.wireless.Epilepsy.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.jar.Attributes;

import info.wireless.Epilepsy.R;


public class OneFragment extends Fragment{
    public TextView jina,number1,number2;
    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_one, container, false);
        jina= (TextView) view.findViewById(R.id.nameyenyewe);
        number1= (TextView) view.findViewById(R.id.relativephone);
        number2= (TextView) view.findViewById(R.id.consulantphone);
        SharedPreferences settings =  getActivity().getSharedPreferences(TwoFragment.EPILEPSYPREFS, Context.MODE_PRIVATE);
        String name = settings.getString(TwoFragment.Name, "Empty");
        String phone1 = settings.getString(TwoFragment.Phone1, "Empty");
        String phone2 = settings.getString(TwoFragment.Phone2, "Empty");

         if(!name.equals("Empty") && !phone1.equals("Empty") && !phone2.equals("Empty")){
             jina.setText(name);
             number1.setText(phone1);
             number2.setText(phone2);
         }else{
            String empty="-Null-";
            jina.setText(empty);
            number1.setText(empty);
            number2.setText(empty);
        }
        return view;
    }

}
