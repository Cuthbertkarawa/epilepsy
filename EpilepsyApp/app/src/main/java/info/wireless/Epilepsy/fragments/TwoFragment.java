package info.wireless.Epilepsy.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import info.wireless.Epilepsy.R;
import info.wireless.Epilepsy.activity.SimpleTabsActivity;


public class TwoFragment extends Fragment {
    private EditText inputName, inputphone1, inputphone2,messageeditor;
    private TextInputLayout inputLayoutName, inputLayoutphone1, inputLayoutphone2,inputLayoutmessage;
    public static final String Name = "nameKey";
    public static final String Phone1 = "phoneKey";
    public static final String Phone2 = "phone2Key";
    public static final String Message = "messageKey";
    public static final String EPILEPSYPREFS = "Epilepsyprefs";
    public TextView jina,number1,number2,sms;
    SharedPreferences sharedpreferences;

    private Button inputsave;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_two, container, false);
        inputLayoutName = (TextInputLayout)view.findViewById(R.id.input_layout_name);
        inputLayoutphone1 = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        inputLayoutphone2 = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        inputLayoutmessage= (TextInputLayout) view.findViewById(R.id.input_layout_messsage);

        inputName = (EditText) view.findViewById(R.id.input_name);
        inputphone1 = (EditText) view.findViewById(R.id.input_phone1);
        inputphone2 = (EditText) view.findViewById(R.id.input_phone2);
        messageeditor= (EditText) view.findViewById(R.id.inputmessage);
        inputsave = (Button) view.findViewById(R.id.btn_signup);

        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputphone1.addTextChangedListener(new MyTextWatcher(inputphone1));
        inputphone2.addTextChangedListener(new MyTextWatcher(inputphone2));
        messageeditor.addTextChangedListener(new MyTextWatcher(messageeditor));

        sharedpreferences = getActivity().getSharedPreferences(EPILEPSYPREFS, Context.MODE_PRIVATE);
        inputsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
        SharedPreferences settings =  this.getActivity().getSharedPreferences(EPILEPSYPREFS, Context.MODE_PRIVATE);
        String name = settings.getString(Name, "");
        String phone1 = settings.getString(Phone1, "");
        String phone2 = settings.getString(Phone2, "");
        String Messagetuma=settings.getString(Message,"");
        jina= (TextView) view.findViewById(R.id.input_name);
        number1= (TextView) view.findViewById(R.id.input_phone1);
        number2= (TextView) view.findViewById(R.id.input_phone2);
        sms= (TextView) view.findViewById(R.id.inputmessage);
        jina.setText(name);
        number1.setText(phone1);
        number2.setText(phone2);
        sms.setText(Messagetuma);

        return view;
    }
    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateName()) {
            return;
        }else

        if (!validatePhone()) {
            return;
        }else

        if (!validatePhone2()) {
            return;
        }else {

           SaveData(inputName .getText().toString(),inputphone1.getText().toString(),inputphone2.getText().toString(),messageeditor.getText().toString());
           Toast.makeText(getActivity().getApplicationContext(), "Successful Save!", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(getActivity(), SimpleTabsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);

                }
            },500);
    }
    }


    private void SaveData(String name, String phone1, String phone2,String Messagetuma ) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Name, name);
        editor.putString(Phone1, phone1);
        editor.putString(Phone2, phone2);
        editor.putString(Message,Messagetuma).apply();

        Log.i("SAVED DATA","Name: "+name+" phone`:"+phone1+" phone2:"+phone2+" Message:"+Message);
    }

    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(inputName);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatePhone() {
        String phoneNo = inputphone1.getText().toString().trim();

        if (phoneNo.matches("\\d{12}")) {
            inputLayoutphone1.setError(getString(R.string.err_msg_phone));
            requestFocus(inputphone1);
            return false;
        } else {
            inputLayoutphone1.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validatePhone2() {
        String phoneNo = inputphone2.getText().toString().trim();

        if (phoneNo.matches("\\d{11}")) {
            inputLayoutphone2.setError(getString(R.string.err_msg_phone));
            requestFocus(inputphone2);
            return false;
        } else {
            inputLayoutphone1.setErrorEnabled(false);
        }

        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_phone1:
                    validatePhone();
                    break;
                case R.id.input_phone2:
                    validatePhone2();
                    break;
            }
        }


    }}
